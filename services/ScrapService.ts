import * as puppeteer from "puppeteer";
import * as natural from "natural";
import * as jsonfile from "jsonfile";
import * as path from "path";
const sw = require("stopword");
let nlp = require("compromise");
const JSON_FILE = path.join(__dirname, "users.json");

export class ScrapService {
  public constructor() {}

  // scrap all the text with puppeteer
  private async scrap(URL: string): Promise<string> {
    try {
      // [CODE REVIEW] Optionally can try cheerio. But I think puppeteer is a better choice in this case
      const browser = await puppeteer.launch();
      const page = await browser.newPage();
      await page.goto(URL);
      let WORDS = await page.evaluate(() => {
        let items = Array.from(
          document.querySelectorAll("span,p,li,h1,h2,h3,h4,h5,h6,article")
        );
        return items.map((e: any) => {
          return e.innerText;
        });
      });
      console.log("words scrapped!");
      return WORDS.join(" ");
    } catch (e) {
      // Manage error of not being able to scrap
      return e;
    }
  }

  // saving words to users.json and returning the wordlist as a promise
  public async logScrapResults(URL: string, username: string): Promise<{}> {
    const WORDS = await this.meaningfulWords(URL);

    let wordList = {};

    // create dictionary {"words": "frequency"}
    for (let word of WORDS) {
      if (wordList[word]) {
        wordList[word] += 1;
      } else {
        wordList[word] = 1;
      }
    }
    console.log(wordList);
    console.log("words tokenized!");

    // remove duplicated words in the array
    // let duplicatesRemoved = Array.from(new Set(WORDS));
    // await this.saveWordsForUser(duplicatesRemoved, username);
    // console.log(wordList);
    // console.log("word list above")

    return wordList;
  }

  //using compromise.js to distinguish meaningful words and output a string array of words
  public async meaningfulWords(URL: string): Promise<string[]> {
    try {
      let content = await this.scrap(URL);
      // console.log(content);

      // detect language with franc

      // unwanted words: location, people or institution (noun)
      let doc = nlp(content);
      let unwantedWords = doc.topics().data();
      console.log(unwantedWords);

      // remove unwanted words from the text
      for (let word of unwantedWords) {
        content = content.replace(word.text, "");
      }
      // console.log(content);
      // tokenize
      let tokenizer = new natural.TreebankWordTokenizer();
      let words = tokenizer.tokenize(content);

      // remove stop words
      words = sw.removeStopwords(words);

      console.log(words);
      // remove numbers and signs with REGEX
      words = words.filter(elem => /^[a-zA-Z]/.test(elem));

      // remove only one character
      words = words.filter(elem => elem.length > 1);

      // remove fullstop
      words = words.map(elem => elem.replace(".", ""));

      // word stemming
      // words = words.map(elem=>natural.PorterStemmer.stem(elem));
      // words = words.map(elem=>stemmer(elem));

      // to lowercase
      words = words.map(x => x.toLowerCase());

      console.log(words);
      return words;
    } catch (e) {
      throw new Error("Error!");
    }
  }

  // save words to user.json
  // [CODE REVIEW] Remove unused code
  public async saveWordsForUser(words: string[], username: string) {
    try {
      // read our data case

      let data = await jsonfile.readFile(JSON_FILE);

      // adding new words to the 0th (first) user (hard-coded)
      for (let word of words) {
        let i = data.findIndex((x: Object) => x["username"] === username);
        console.log(i);
        if (!(word in data[i].acquiredWords)) {
          data[i].acquiredWords[word] = {};
        }
      }

      // write new database into the json file
      await jsonfile.writeFile(JSON_FILE, data);
      console.log("Words saved for user");
    } catch (e) {
      throw new Error("cannot save words");
    }
  }

  // save chosen words
  public async saveWords(words: string[], username: string) {
    try {
      // read our data case
      let data = await jsonfile.readFile(JSON_FILE);

      // adding new words to the 0th (first) user (hard-coded)
      let i = data.findIndex((x: Object) => x["username"] === username);
      if (i) {
        for (let word of words) {
          data[i]["acquiredWords"][word] = {};
        }
      }
      // write new database into the json file
      await jsonfile.writeFile(JSON_FILE, data);
      console.log("Words saved for user");
    } catch (e) {
      throw new Error("cannot save words");
    }
  }

  // delete unwanted words
  public async deleteWords(words: string[], username: string) {
    try {
      // read our data case
      let data = await jsonfile.readFile(JSON_FILE);

      // adding new words to the 0th (first) user (hard-coded)
      for (let word of words) {
        let i = data.findIndex((x: Object) => x["username"] === username);
        if (word in data[i].acquiredWords) {
          delete data[i].acquiredWords[word];
        }
      }

      // write new database into the json file
      await jsonfile.writeFile(JSON_FILE, data);
      console.log("Words deleted for user");
    } catch (e) {
      throw new Error("cannot delete words");
    }
  }
}
