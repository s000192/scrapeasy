const fetch = require("node-fetch");

export class OxfordDictionary {
  private app_id: string;
  private app_key: string;
  private option: object;
  private fields: string;
  private strictMatch: string;
  constructor() {
    // [CODE REVIEW] Learn how to `dotenv` in module 3
    this.app_id = "4b4c1112";
    this.app_key = "d6b2408b089ab4a68f7a6222aa12de0f";
    this.fields = "pronunciations";
    this.strictMatch = "false";
    this.option = {
      headers: {
        Accept: "application/json",
        app_id: this.app_id,
        app_key: this.app_key,
      }
    };
  }

  async getWordData(word: string): Promise<object> {
    let url =
      "https://od-api.oxforddictionaries.com:443/api/v2/entries/en-gb/" +
      word + '?fields=' + this.fields
      "&strictMatch=" + this.strictMatch;
    let data = await fetch(url, this.option);
    data = await data.json();
    let explanation: object = {};
    let id = data["id"];
    let nature =
      data["results"][0]["lexicalEntries"][0]["lexicalCategory"]["text"];
    let definition =
      data["results"][0]["lexicalEntries"][0]["entries"][0]["senses"][0][
        "definitions"
      ][0];
    // function capitalizeFirstLetter(string: string): string {
    //   return string.charAt(0).toUpperCase() + string.slice(1);
    // }
    // id = capitalizeFirstLetter(id);
    // definition = capitalizeFirstLetter(definition);
    explanation["id"] = id;
    explanation["nature"] = nature;
    explanation["definition"] = definition;
    return explanation;
  }
}
