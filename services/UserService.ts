import * as jsonfile from "jsonfile";
import * as path from "path";

const usersJSON = path.join(__dirname, "users.json");

export interface User {
  username: string;
  password: string;
}

export class UserService {
  constructor() {}

  public async createUser(newUser: User): Promise<object | any> {
    const users: User[] = await jsonfile.readFile(usersJSON);
    const existingUser = users.find(
      user => newUser.username === user["username"]
    );
    if (existingUser) {
      throw new Error(`User ${newUser.username} already exists`);
    } else {
      newUser["acquiredWords"] = {};
      users.push(newUser);
      jsonfile.writeFile(usersJSON, users, { spaces: 2 });
    }
  }
}
