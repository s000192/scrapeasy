import * as jsonfile from 'jsonfile'
import * as path from 'path'


const JSON_FILE = path.join(__dirname, 'users.json')

export class SummaryService {

    //private users: []
    constructor() {
        //        this.users = jsonfile.readFileSync(JSON_FILE).then((data)=>{
        //            this.users = data.users;
        //        })
    }

    async listUser(): Promise<any> {
    // [CODE REVIEW]          ^^^ define an interface for User instead of any, User[]
        return await jsonfile.readFile(JSON_FILE);

    }

    async showUsername(ExistingUser: string) {
    // [CODE REVIEW]   ^^^^^^^^^^^^ lower case first
        const call = await this.listUser();
        const identifyUser = call.find((user: any) => ExistingUser == user.username)
        if (identifyUser) {
            return identifyUser.username;
        }

    }
    async wordsLearned(ExistingUser: string) {
        const call = await this.listUser();
        const identifyUser = call.find((user: any) => ExistingUser == user.username)
        if (identifyUser) {
            return identifyUser.wordsLearned;
        }
    }
    //
    async mostVisitedWords(ExistingUser: string) {
        const call = await this.listUser();
        const identifyUser = call.find((user: any) => ExistingUser == user.username)
        const seenNumber = identifyUser.acquiredWords
        let seenNo: object[] = []
        if (identifyUser) {
            for (let word in seenNumber) {
                let wordObj = {}
                if (seenNumber[word]['seen']!=null) {
                wordObj["word"] = word;
                wordObj["seen"] = seenNumber[word]['seen'];
                seenNo.push(wordObj);
                }
            }
            const sortedSeenNo = seenNo.sort((a, b) => {
                if (a['seen'] > b['seen']) return -1;
                if (a['seen'] < b['seen']) return 1;
                return 0;

            })
            if (sortedSeenNo.length >=5) {
             let Arr = sortedSeenNo.slice(0, 5);
             //console.log(Arr);
             // [CODE REVIEW] Return array instead of string. Service should not handle "presentation"
             return `${Arr[0]['word']}, ${Arr[1]['word']}, ${Arr[2]['word']}, ${Arr[3]['word']}, ${Arr[4]['word']}`;
            } else {
            //let Arr = sortedSeenNo.slice(0, sortedSeenNo.length);
            // [CODE REVIEW] If above return array, here just return empty array 
                return 'not enough words for evaluation';
            }
        } else {
            throw Error('user not found');
        }

    }

    async correctPercentage(ExistingUser: string) {
        const call = await this.listUser();
        const identifyUser = call.find((user: any) => ExistingUser == user.username)
        const wordsobj = identifyUser.acquiredWords
        let attempted = 0
        let correct = 0
        if (identifyUser) {
            for (let word in wordsobj) {
                if (wordsobj[word]['attempted']!=null){
                attempted += wordsobj[word]['attempted'];
                }
                if (wordsobj[word]['correct']!=null){
                correct += wordsobj[word]['correct'];
                }
            }
            if (attempted !== 0) {
            const correctPer = (correct / attempted) * 100;
            const num = correctPer.toFixed(2);
            //console.log(attempted);
            //console.log(correct);
            //console.log(wordsobj['wtf']['attempted']);
            //console.log(correct);
            return `${num}`;
            } else {
                return 'you have not attempted the quiz yet';
            }
        } else {
            throw Error('user not found');
        }
        
    }

    async MostFailedAttempts(ExistingUser: string) {
        const call = await this.listUser();
        const identifyUser = call.find((user: any) => ExistingUser == user.username)
        const wordsObje = identifyUser.acquiredWords
        let failedNo: object[] = []

        if (identifyUser) {
            for (let word in wordsObje) {
                let attempted = 0
                let correct = 0
                if (wordsObje[word]['attempted']!=null) {
                attempted += wordsObje[word]['attempted'];
                }
                if (wordsObje[word]['correct']!=null) {
                correct += wordsObje[word]['correct'];
                }
                const wordFailedAttempt = attempted - correct;
                let wordObj = {}
                wordObj["word"] = word;
                wordObj["failedAttempts"] = wordFailedAttempt;
                failedNo.push(wordObj)

            }
            const sortedFailNo = failedNo.sort((a, b) => {
                if (a['failedAttempts'] > b['failedAttempts']) return -1;
                if (a['failedAttempts'] < b['failedAttempts']) return 1;
                return 0;

            })
            if (sortedFailNo.length >= 5) {
            let Arr = sortedFailNo.slice(0, 5);
            //console.log(Arr);
            return `${Arr[0]['word']}, ${Arr[1]['word']}, ${Arr[2]['word']}, ${Arr[3]['word']}, ${Arr[4]['word']}`;
            } else {
                return 'not enough words for evaluation';
            }
        }else {
            throw Error('user not found');
        }

    }
    async MostQuizAttempts(ExistingUser: string) {
        const call = await this.listUser();
        const identifyUser = call.find((user: any) => ExistingUser == user.username);
        const wordsObje = identifyUser.acquiredWords;
        let attemptNo: object[] = []

        if (identifyUser) {
            for (let word in wordsObje) {
                let attempted = 0
                let wordObj = {}

                if (wordsObje[word]['attempted']!=null) {
                attempted += wordsObje[word]['attempted'];
                wordObj["word"] = word;
                wordObj["Attempts"] = attempted;
                attemptNo.push(wordObj);
                }

            }
            const sortedAttemptNo = attemptNo.sort((a, b) => {
                if (a['Attempts'] > b['Attempts']) return -1;
                if (a['Attempts'] < b['Attempts']) return 1;
                return 0;

            })
            if (sortedAttemptNo.length >=5) {
            let Arr = sortedAttemptNo.slice(0, 5);
            //console.log(Arr);
            return `${Arr[0]['word']}, ${Arr[1]['word']}, ${Arr[2]['word']}, ${Arr[3]['word']}, ${Arr[4]['word']}`;
            }else {
                return 'not enough words for evaluation';
            }
        }else {
            throw Error('user not found');
        }

    }
}

    //const Ser = new SummaryService();
    //Ser.listUser();
    //Ser.showUsername('Larry');
    //Ser.wordsLearned('Larry');
    //Ser.mostVisitedWords('Larry');
    //Ser.correctPercentage('Larry');
    //Ser.MostFailedAttempts('Larry');
    //Ser.MostQuizAttempts('Larry')
