import * as jsonfile from "jsonfile";
import * as path from "path";
import { OxfordDictionary } from "./OxfordDictionaryAPI";
import { imageSearch } from "./imageSearchAPI";

// [CODE REVIEW] Move this variable to the constructor of StudyService
let dictionary = new OxfordDictionary();

const usersJSON = path.join(__dirname, "users.json");
const wordBank = path.join(__dirname, "wordBank.json");

export class StudyService {
  constructor() {}

  private async extractAcquiredWords(
    CurrentUser: string
  ): Promise<object | any> {
    const data: object[] = await jsonfile.readFile(usersJSON);
    let userData = data.find(user => user["username"] === CurrentUser);
    if (userData) {
      return userData["acquiredWords"];
    }
    throw new Error("no this user");
  }

  private async generateWordsList(user: string): Promise<object[]> {
    const acquiredWords = await this.extractAcquiredWords(user);
    const wordsList: object[] = [];
    // create an array of object
    for (let word in acquiredWords) {
      let wordObject: object = {};
      wordObject["id"] = word;
      wordObject["seen"] = acquiredWords[word]["seen"];
      wordsList.push(wordObject);
    }
    return wordsList;
  }

  public async sortWordsList(user: string): Promise<object[]> {
    const wordsList = await this.generateWordsList(user);
    // sort according to seen count
    const sortedWordsList: object[] = wordsList.sort((a, b) => {
      if (a["seen"] < b["seen"]) return -1;
      if (a["seen"] > b["seen"]) return 1;
      return 0;
    });
    console.log(sortedWordsList);
    return sortedWordsList;
  }

  public async accessCardContent(word: string,currentUser: string): Promise<object> {
    let data: object[] = await jsonfile.readFile(wordBank);
    const item = data.find(item => item["id"] === word);
    if (item) {
      console.log(item);
      return item;
    } else {
      let newWord = await dictionary.getWordData(word);
      newWord["url"] = await imageSearch(word);
      data.push(newWord);
      jsonfile.writeFile(wordBank, data, { flag: "w" }, function(err) {
        if (err) console.error(err);
      });
      console.log(newWord);
      this.seenCard(word, currentUser);
      return newWord;
    }
  }

  async seenCard(word: string, currentUser: string) {
    // set the first_learned and seen count of the card
    const data: object[] = await jsonfile.readFile(usersJSON);
    let userData = data.find(user => user["username"] === currentUser);
    if (userData) {
      if (
        !data[data.indexOf(userData)]["acquiredWords"][word]["firstLearned"]
      ) {
        let now = new Date();
        // [CODE REVIEW] Use moment.js: moment().format('YYYY-MM-DD')
        let date = `${now.getFullYear()}-${now
          .getMonth()
          .toString()
          .padStart(2, "0")}-${now
          .getDate()
          .toString()
          .padStart(2, "0")}`;
        data[data.indexOf(userData)]["acquiredWords"][word][
          "firstLearned"
        ] = date;
      }
      data[data.indexOf(userData)]["acquiredWords"][word]["seen"]++;
    }

    // [CODE REVIEW] Should use async/await style to catch any errors
    jsonfile.writeFile(usersJSON, data, { flag: "w" }, function(err) {
      if (err) console.error(err);
    });
  }
}
