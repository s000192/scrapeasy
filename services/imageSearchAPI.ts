//const rp = require("request-promise");
//const cheerios = require('cheerio');
const fetch = require("node-fetch");


export async function imageSearch(word:string){
 

 const app_key = '3e15ea22ed294b039f1e4d559b1cf155';

  let option = {
    headers:{
      'Ocp-Apim-Subscription-Key': app_key,
    }
  }

  const url = `https://api.cognitive.microsoft.com/bing/v7.0/images/search/?q=${word}&size=medium` 
 
  let data = await fetch(url, option);
  data = await data.json();

  return data.value[0]['contentUrl']
}

// imageSearch('apple');