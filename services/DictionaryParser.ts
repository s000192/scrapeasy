// const rp = require("request-promise");
// const cheerio = require('cheerio');


// npm install --save request request-promise cheerio puppeteer

// // Scrape through unofficial Google Dictionary API
// export async function dictionaryParser(word: string){
//     const url = "https://mydictionaryapi.appspot.com/?define=" + word;
//     let data = await rp(url);
//     data = JSON.parse(data);
//     return data;
// };

// Scrape from Cambridge Dictionary
// export async function dictionaryParser(word: string){
//     const url = "https://dictionary.cambridge.org/dictionary/english/" + word;
//     const data = await rp(url);
//     const $ = cheerio.load(data);
//     const detail = $('.sense-body > div:first-child b.def').text();
//     return detail
// };

// const wordsList = [{"word": "apple", "seen": 2}];

// const wordsList = [{"word": "apple", "seen": 2},{"word":"orange", "seen":3},{"word":"pear", "seen":8}, {"word":"strawberry", "seen":3}, {"word":"friend", "seen":3}, {"word":"girl", "seen":3}, {"word":"guys", "seen":3}];

// const wordsList = [{"word": "apple", "seen": 2},{"word": "apple", "seen": 2},{"word": "apple", "seen": 2},{"word": "apple", "seen": 2},{"word": "apple", "seen": 2},{"word": "apple", "seen": 2},{"word": "apple", "seen": 2},{"word": "apple", "seen": 2},{"word": "apple", "seen": 2},{"word": "apple", "seen": 2},{"word": "apple", "seen": 2},{"word": "apple", "seen": 2},{"word": "apple", "seen": 2},{"word": "apple", "seen": 2},{"word": "apple", "seen": 2},{"word": "apple", "seen": 2}];

// (async function accessCardContent() {
//         let start = new Date();
//         const dictionaryContent = await Promise.all(
//           wordsList.map(word => dictionaryParser(word["word"]))
//         );
//         let end = new Date();
//         let time = end.getTime() - start.getTime();
        
//         console.log(dictionaryContent);
//         console.log(time);
//       })();

