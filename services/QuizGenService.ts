import * as jsonfile from "jsonfile";
import * as path from "path";
import { imageSearch } from "./imageSearchAPI";

const JSON_FILE = path.join(__dirname, "wordBank.json");
const USER_FILE = path.join(__dirname, "users.json");

export class QuizGenService {
    constructor() { }

    async quizImageGen(word: string) {
        //let url = [];
        const wordBank:object[] = await jsonfile.readFile(JSON_FILE);
        let checkWords = wordBank.find(existingWord => 
                existingWord['id'] === word
        );
        //console.log(checkWords);
        if (checkWords) {
            //there is existing link in json file
            for (let wordBankWord of wordBank) {
                if (wordBankWord['id'] == word) {
                    return wordBankWord['url'];
                }
            }
        } else {
            //no image link in json file
            const result = await imageSearch(word);
            return result;
        }
    }

    async wrongQuizImage(word: string) {
        const wordBank = await jsonfile.readFile(JSON_FILE);
        // [CODE REVIEW] 1) Should use an cloned array (wordBank.slice()) to pop the result in an while loop
        // 2) Filter the array and then Math.random() 
        //    Example: const wrongQuizCandidates = wordBank.filter(w => w.id != word && w.url);
        //             return wrongQuizCandidates[Math.floor(Math.random() * wrongQuizCandidates.length)]

        //every word in wordBank
        for (let wordBankWord of wordBank) {
            //skip if input == id
            if (wordBankWord.id == word) {
                continue;
            } else {
                for (let i = 0; i < wordBank.length; i++) {
                    let num = Math.floor(Math.random() * wordBank.length);
                    if (wordBank[num].url) {
                        return wordBank[num].url;
                    } else {
                        continue;
                    }
                }
            }
        }
    }

    async quizWordGenerator(currentUser: string) {
        let acqWordsArr: string[] = [];
        let quizArr: string[] = [];
        const UserWordBank: object[] = await jsonfile.readFile(USER_FILE);
        const userData = UserWordBank.find(
            (user) => user["username"] === currentUser
        );

        if (userData) {
            const acquiredWords =
                UserWordBank[UserWordBank.indexOf(userData)]["acquiredWords"];
            for (let item in acquiredWords) {
                acqWordsArr.push(item);
            }
            console.log(acqWordsArr);
            if (acqWordsArr.length >= 5) {
                while (quizArr.length < 5) {
                    let num = Math.floor(Math.random() * Object.keys(acquiredWords).length);
                    if (quizArr.length < 1) {
                        quizArr.push(acqWordsArr[num]);
                    } else {
                        if (quizArr.indexOf(acqWordsArr[num]) == -1) {
                            quizArr.push(acqWordsArr[num]);
                        } else {
                            continue;
                        }
                    }
                }
            } else {
                while (quizArr.length < acqWordsArr.length) {
                    let num = Math.floor(Math.random() * Object.keys(acquiredWords).length);
                    if (quizArr.length < 1) {
                        quizArr.push(acqWordsArr[num]);
                    } else {
                        if (quizArr.indexOf(acqWordsArr[num]) == -1) {
                            quizArr.push(acqWordsArr[num]);
                        } else {
                            continue;
                        }
                    }
                }
            }

        }
        console.log(quizArr);
        return quizArr;
    }

    async quizImgObj(word: string): Promise<any> {
        const correctLink = await this.quizImageGen(word);
        const wrongLink = await this.wrongQuizImage(word);

        const imgPair = { correct: correctLink, wrong: wrongLink };

        if (correctLink == wrongLink) {
            return this.quizImgObj(word);
        } else {
            console.log(imgPair);
            return imgPair;
        }
    }

    async rightAnswerAdd(word: string, user: string) {
        const userBank:object[] = await jsonfile.readFile(USER_FILE);
        // console.log(userBank);
        let userData = userBank.find(existingUser => 
             existingUser["username"] === user
        );
        //console.log(userData);
        if (userData) {
            const acquiredWords = userBank[userBank.indexOf(userData)]["acquiredWords"];
            for (let bankWord in acquiredWords) {
                if (bankWord == word) {
                    if (acquiredWords[bankWord]["attempted"] == null && 
                    acquiredWords[bankWord]["correct"] == null) {
                        acquiredWords[bankWord]["attempted"] = 1;
                        acquiredWords[bankWord]["correct"] = 1;
                        await jsonfile.writeFile(USER_FILE, userBank);
                        // console.log(acquiredWords[bankWord]);
                        return acquiredWords[bankWord];
                    } else {
                    acquiredWords[bankWord]["attempted"] += 1;
                    acquiredWords[bankWord]["correct"] += 1;
                    await jsonfile.writeFile(USER_FILE, userBank);
                    // console.log(acquiredWords[bankWord]);
                    return acquiredWords[bankWord];
                    }
                }
            }
        }
    }

    async wrongAnswerAdd(word: string, user:string) {
        const userBank:object[] = await jsonfile.readFile(USER_FILE);
        // console.log(userBank);
        let userData = userBank.find(existingUser => 
             existingUser["username"] === user
        );
        //console.log(userData);
        if (userData) {
        const acquiredWords = userBank[userBank.indexOf(userData)]["acquiredWords"];
        for (let bankWord in acquiredWords) {
            if (bankWord == word) {
                if (acquiredWords[bankWord]["attempted"] == null) {
                acquiredWords[bankWord]["attempted"] = 1;
                await jsonfile.writeFile(USER_FILE, userBank);
                console.log(acquiredWords[bankWord]);
                return acquiredWords[bankWord];
                } else {
                    acquiredWords[bankWord]["attempted"] += 1;
                await jsonfile.writeFile(USER_FILE, userBank);
                console.log(acquiredWords[bankWord]);
                return acquiredWords[bankWord];
                }
            }
        }
    }
    }
}

//const quizGenService = new QuizGenService();

//quizGenService.quizImageGen('banana');
//wrongQuizImage('banana');
//quizGenService.quizWordGenerator('jackson');
//quizGenService.quizImgObj('apple');
//quizGenService.rightAnswerAdd("help", "Larry");
//quizGenService.wrongAnswerAdd('feedback', 'Larry');
