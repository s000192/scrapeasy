import * as express from "express";
import * as bodyParser from "body-parser";
import * as expressSession from "express-session";
import * as morgan from "morgan";
import * as jsonfile from "jsonfile";
import * as path from "path";
// import { Request, Response } from "express";
import { UserRouter } from "./routers/UserRouter";
import { UserService } from "./services/UserService";
import { StudyRouter } from "./routers/StudyRouter";
import { StudyService } from "./services/StudyService";
import { SummaryService } from "./services/summaryService";
import { SummaryRouter } from "./routers/summaryRouter";
import { ScrapService } from "./services/ScrapService";
import { ScrapRouter } from "./routers/ScrapRouter";
import { QuizRouter } from "./routers/QuizRouter"
import {QuizGenService} from "./services/QuizGenService"

const app = express();

app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Session and Cookies
app.use(
  expressSession({
    secret: "Learning vocabulary with the power of NLP",
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false, maxAge: 604800000 }
  })
);

function loginGuard(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  if (req.session && req.session.isLoggedIn) {
    next();
  } else {
    res.sendFile(path.join(__dirname + "/public/login/login.html"));
  }
}

// Login & Sign up
app.get("/", async (req, res) => {
  // [CODE REVIEW] If logged in, redirect to main page. Else, redirect to login page
  res.sendFile(path.join(__dirname, "/public/login/login.html"));
});

app.get("/login", async (req, res) => {
  res.sendFile(path.join(__dirname, "/public/login/login.html"));
});

app.get("/signup", async (req, res) => {
  res.sendFile(path.join(__dirname, "/public/signup/signup.html"));
});

app.get("/transition", async (req, res) => {
  res.sendFile(path.join(__dirname, "/public/transition/transition.html"));
});

app.get("/logout", async (req, res) => {
  if (req.session) {
    req.session.isLoggedIn = false;
  }
  res.sendFile(path.join(__dirname, "/public/login/login.html"));
});

app.post("/login", async (req, res) => {
  const { username, password } = req.body;
  const users: object[] = await jsonfile.readFile(
    path.join(__dirname, "/services/users.json")
  );
  const user = users.find(
    user => user["username"] === username && user["password"] === password
  );
  if (req.session && user) {
    req.session.isLoggedIn = true;
    req.session.currentUser = username;
    console.log(req.session);
    console.log(`The current user is ${username}`);
    res.redirect("/scrap/");
  } else {
    res.sendFile(path.join(__dirname + "/public/login/login.html"));
  }
});

// Routers
const userService = new UserService();
const userRouter = new UserRouter(userService);

const scrapService = new ScrapService();
const scrapRouter = new ScrapRouter(scrapService);

const studyService = new StudyService();
const studyRouter = new StudyRouter(studyService);

const summaryService = new SummaryService();
const summaryRouter = new SummaryRouter(summaryService);

app.use("/users", userRouter.router());
app.use("/scrap", loginGuard, scrapRouter.router());
app.use("/study", loginGuard, studyRouter.router());
app.use("/summary", loginGuard, summaryRouter.router());

app.use(express.static("public"));

// [CODE REIVEW] Better use a URL prefix to prevent files collision
app.use(/* '/portal', */ loginGuard, express.static("protected"));

const quizGenService = new QuizGenService();
const quizRouter = new QuizRouter(quizGenService);
app.use("/quiz", loginGuard, quizRouter.router());

const PORT = 8080;

app.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}/`);
});
