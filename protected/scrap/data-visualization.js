class DataVisualization {
    constructor() {
        this.unwantedWords = [],
        this.wordsToSave = []
    }

    restructure(wordList) {
        let newWordList = [];
        const words = Object.keys(wordList);
        for (let s of words) {
            let newWord = { word: "", size: "" };
            newWord.word = s;
            newWord.size = wordList[s] + "";
            newWordList.push(newWord);
        }
        newWordList.sort(function (obj1, obj2) {
            return obj2.size - obj1.size;
        })
        return newWordList;
    }

    createBarPlot(wordList) {
        // set the dimensions and margins of the graph
        var margin = { top: 20, right: 30, bottom: 40, left: 90 },
            width = 900 - margin.left - margin.right,
            height = wordList.length * 10 - margin.top - margin.bottom;

        // append the svg object to the body of the page
        var svg = d3.select("#word-bar")
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");

        let maxF = this.maxFrequency(wordList);
        console.log(maxF);

        // Add X axis
        var x = d3.scaleLinear()
            .domain([0, maxF])
            .range([0, width]);
        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x))
            .selectAll("text")
            .attr("transform", "translate(-10,0)rotate(-45)")
            .style("text-anchor", "end");

        // Y axis
        var y = d3.scaleBand()
            .range([0, height])
            .domain(wordList.map(function (d) { return d.word; }))
            .padding(.1);
        svg.append("g")
            .call(d3.axisLeft(y))

        //create tooltip for barchart
        let tooltip = this.createTooltip("#word-bar")

        //Bars
        svg.selectAll("myRect") // selectAll("rect.selected")
            .data(wordList)
            .enter()
            .append("rect")
                .attr("class", "selected")
                .attr("id", function(d) { return d.word; })
                .attr("y", function (d) { return y(d.word); })
                .attr("height", y.bandwidth())
                .attr("fill", "#0084D5")
                // no bar at the beginning
                .attr("x", function(d) { return width - x(0); }) // always equal to 0
                .attr("width", function(d) { return x(0); })
                // add tooltip
                .on("mouseover", () => {return this.mouseover(tooltip)})
                .on("mousemove", (d) => {return this.mousemove(tooltip, d);})
                .on("mouseout", () => {this.mouseout(tooltip)})
                .on("click", this.click)

        // animation
        svg.selectAll("rect")
            .transition()
            .duration(200)
            .attr("x", function(d) { return x(0); }) 
            .attr("width", function(d) { return x(d.size); })
            .delay(function (d, i) { console.log(i); return (i * 40) })
    }

    createWordCloud(wordList) {
        wordList = this.normalize(wordList);

        // set the dimensions and margins of the graph
        var margin = { top: 10, right: 10, bottom: 10, left: 10 },
            width = 900 - margin.left - margin.right,
            height = 900 - margin.top - margin.bottom;

        // append the svg object to the body of the page
        var svg = d3.select("#word-cloud").append("svg")
            .data(wordList)
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")")
                .on("mouseover", () => {return this.mouseover(tooltip)})
                .on("mousemove", (d) => {return this.mousemove(tooltip, d);})
                .on("mouseout", () => {this.mouseout(tooltip)})
                .on("click", this.click);

        //create tooltip for barchart
        let tooltip = this.createTooltip("#word-cloud")

        // Constructs a new cloud layout instance. It run an algorythm to find the position of words that suits your requirements
        var layout = d3.layout.cloud()
            .size([width, height])
            .words(wordList.map(function (d) { return { text: d.word, size: d.size }; }))
            .padding(5)
            .fontSize(function (d) { return d.size; })
            .on("end", draw);
        layout.start();

        function draw(words) {
            svg
                .append("g")
                .attr("id", function(d) { return d.text; })
                .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
                .selectAll("text")
                .data(words)
                .enter().append("text")
                .style("font-size", function (d) { return d.size; })
                .style("fill", "#0084D5")
                .style("font-family", "Impact")
                .attr("text-anchor", "middle")
                .attr("transform", function (d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                })
                .text(function (d) { return d.text; });
        }
    }

    createTooltip(area) {
        // [CODE REVIEW] Just use HTML is fine... No need everything d3.js. Example: d3.select('div#tooltip')
        return d3.select(area)
        .append("div")
        .style("position", "absolute")
        .style("z-index", "10")
        .style("visibility", "hidden")        
        .style("opacity", 0)
        .attr("class", "tooltip")
        .style("background-color", "white")
        .style("border", "solid")
        .style("border-width", "2px")
        .style("border-radius", "5px")
        .style("padding", "5px");       
    }

    mouseover(tooltip) {
        return tooltip.style("visibility", "visible")
        .style("opacity", 1);
    }

    mousemove(tooltip, d) {
        if (d3.select(d3.event.target).classed("selected")) {
            return tooltip
            .html(`Click to save<br>this word (${d.word})`)
            .style("top", (event.pageY-10)+"px").style("left",(event.pageX+10)+"px");
        }
        else if (d3.select(d3.event.target).classed("toSave")) {
            return tooltip
            .html(`Click to have<br>this word (${d.word}) back`)
            .style("top", (event.pageY-10)+"px").style("left",(event.pageX+10)+"px");
        }
        
    }

    mouseout(tooltip) {
        return tooltip.style("visibility", "hidden");
    }

    click = () => {
        // check if clicked
        if (d3.select(d3.event.target).classed("selected")) {
            // add to unwanted array
            console.log(d3.select(d3.event.target).attr("id"));
            this.wordsToSave.push(d3.select(d3.event.target).attr("id"));
            d3.select(d3.event.target)
            // bar color change to red
            .attr("fill", "#FF4746")
            // remove class selected
            .classed("selected", false)
            // add class unwanted
            .classed("toSave", true);


        }

        else if (d3.select(d3.event.target).classed("toSave")) {
            // delete from unwanted array
            let index = this.wordsToSave.indexOf(d3.select(d3.event.target).attr("id"));
            this.wordsToSave.splice(index, 1);

            d3.select(d3.event.target)
            // bar color change to blue
            .attr("fill", "#0084D5")
            // remove class unwanted
            .classed("toSave", false)
            // add class selected
            .classed("selected", true);
        }
    }

    maxFrequency(wordList) {
        // d3.max(wordList.map(w => w.size))
        return wordList.reduce(function(acc, cur) {
            return Math.max(acc, cur.size);
        }, 0);
    }

    normalize(wordList) {
        let ratio = this.maxFrequency(wordList) / 100;
        console.log(`The ratio is ${ratio}`)
        wordList = wordList.map(v => {
            v.size = Math.round(v.size/ratio);
            return v;
        });
        return wordList;
    }
}