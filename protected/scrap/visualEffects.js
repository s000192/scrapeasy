class visualEffects {
  constructor() {
    this.form = document.querySelector("#URL-form");
    this.submitButton = document.querySelector("#scrapButton");
    this.spinner = document.querySelector(".spinner-border");
    this.logo = document.querySelector("#logo");
    this.scrap = document.querySelector("#scrap-form");
    this.form.addEventListener("submit", () => {
      this.postSubmissionEffect();
    });
  }

  postSubmissionEffect() {
    this.submitButton.classList.add("d-none");
    this.logo.classList.add("d-none");
    this.spinner.classList.remove("d-none");
    this.scrap.classList.remove("h-75");
    this.form.classList.remove("m-5");
    this.form.classList.add("m-3");
  }

  resumeButton() {
    this.submitButton.classList.toggle("d-none");
    this.spinner.classList.toggle("d-none");
  }

  postSaveEffect() {
    this.submitButton.classList.remove("d-none");
    this.logo.classList.remove("d-none");
    this.spinner.classList.add("d-none");
    this.scrap.classList.add("h-75");
    this.form.classList.add("m-5");
    this.form.classList.remove("m-3");
  }

  dataFormSwitch() {
    document.querySelector("#word-cloud").classList.toggle("d-none");
    document.querySelector("#word-bar").classList.toggle("d-none");
    document.querySelector("#switch-button").classList.toggle("btn-success");
    document.querySelector("#switch-button").classList.toggle("btn-danger");
    if (document.querySelector("#switch-button").innerHTML === "Cloud") {
      document.querySelector("#switch-button").innerHTML = "Bar";
    } else {
      document.querySelector("#switch-button").innerHTML = "Cloud";
    }
  }
}
