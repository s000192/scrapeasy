const global = {};

getUsername = async () => {
    const res = await fetch ('/summary/Username')
    const result = await res.json();
    document.querySelector('#userName').innerHTML = result;
}


window.onload = () => {
    global.visualEffects = new visualEffects();
    global.wordList = new WordList();
    global.dataVisualization = new DataVisualization();
    getUsername();
}

function debug(){
    console.log(global.wordList);
    console.log(global.dataVisualization);
}
