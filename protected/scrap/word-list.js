class WordList {
  constructor() {
    this.list = document.querySelector("#word-list");
    this.URLForm = document.querySelector("#URL-form");
    this.URLForm.addEventListener("submit", () => {
      this.loadData(event);
    });
  }

  async loadData(event) {
    event.preventDefault();
    const URL = document.getElementById("URL").value;
    console.log(URL);
    const res = await fetch("/scrap/words", {
      method: "POST",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify({ URL: URL })
    });
    this.words = await res.json(); // Text to objects // JSON.parse
    console.log(this.words);
    this.render();
    global.visualEffects.resumeButton();
    this.newWordList = global.dataVisualization.restructure(this.words);
    console.log(this.newWordList);
    global.dataVisualization.createBarPlot(this.newWordList);
    console.log("BarPlot done");
    global.dataVisualization.createWordCloud(this.newWordList);
    // this.URLForm.removeEventListener("submit", );
  }

  editUser(username) {
    const user = this.words.find(user => user.username === username);
    global.changeUserForm.populateForm(user);
  }

  render() {
    this.list.innerHTML = "";
    this.list.innerHTML += `<div id="config" class="w-100 d-flex flex-row justify-content-start">
    <button id="save-button" type="button" class="btn btn-danger mx-2">Save</button>
    <button id="switch-button" type="button" class="btn btn-primary">Cloud</button>
    </div>
    <div id="word-bar"></div>
    <div id ="word-cloud" class="d-none"></div>`;
    this.saveButton = document.querySelector("#save-button");
    this.saveButton.addEventListener("click", () => this.deleteWords(event));
    this.switch = document.querySelector("#switch-button");
    this.switch.addEventListener("click", () => {
      global.visualEffects.dataFormSwitch();
    });
  }

  async deleteWords(event) {
    // event.preventDefault();
    console.log(global.dataVisualization.wordsToSave);
    const res = await fetch("/scrap/words", {
      method: "PUT",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify(global.dataVisualization.wordsToSave)
    });
    alert("Words saved!");
    this.clearWords();
    global.visualEffects.postSaveEffect();
  }

  clearWords() {
    this.list.innerHTML = "";
  }
}
