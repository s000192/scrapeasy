"use strict";

function initCards() {
  var tinderContainer = document.querySelector("#study");
  var allCards = document.querySelectorAll(".flash-card");
  var newCards = document.querySelectorAll(".flash-card:not(.removed)");

  newCards.forEach(function(card, index) {
    card.style.zIndex = allCards.length - index;
    card.style.transform =
      "scale(" + (20 - index) / 20 + ") translateY(-" + 30 * index + "px)";
    card.style.opacity = (10 - index) / 10;
  });

  tinderContainer.classList.add("loaded");
}

// initCards();

const addCardEffect = el => {
  var hammertime = new Hammer(el);

  hammertime.on("pan", () => {
    el.classList.add("moving");
  });

  //   grabbing effect
  hammertime.on("pan", event => {
    if (event.deltaX === 0) return;
    if (event.center.x === 0 && event.center.y === 0) return;

    var xMulti = event.deltaX * 0.03;
    var yMulti = event.deltaY / 80;
    var rotate = xMulti * yMulti;

    event.target.style.transform =
      "translate(" +
      event.deltaX +
      "px, " +
      event.deltaY +
      "px) rotate(" +
      rotate +
      "deg)";
  });

  //   release the grab
  hammertime.on("panend", async function(event) {
    el.classList.remove("moving");

    var moveOutWidth = document.body.clientWidth;
    var keep = Math.abs(event.deltaX) < 80 || Math.abs(event.velocityX) < 0.5;

    event.target.classList.toggle("removed", !keep);

    if (keep) {
      event.target.style.transform = "";
    } else {
      var endX = Math.max(
        Math.abs(event.velocityX) * moveOutWidth,
        moveOutWidth
      );
      var toX = event.deltaX > 0 ? endX : -endX;
      var endY = Math.abs(event.velocityY) * moveOutWidth;
      var toY = event.deltaY > 0 ? endY : -endY;
      var xMulti = event.deltaX * 0.03;
      var yMulti = event.deltaY / 80;
      var rotate = xMulti * yMulti;

      event.target.style.transform =
        "translate(" +
        toX +
        "px, " +
        (toY + event.deltaY) +
        "px) rotate(" +
        rotate +
        "deg)";
      initCards();
    }
    await global.study.getNextCard();
  });
};

// allCards.forEach(el => {
//   addCardEffect(el);
// });
