class Cards {
  constructor() {
    this.load = 0;
    this.wordList = [];
  }

  initialize = async () => {
    await this.getWordList();
    for (let i = 0; i < 5; i++) {
      await this.getCardContent(this.wordList[this.load]["id"]);
      this.load++;
    }
  };

  getWordList = async () => {
    const res = await fetch("/study/wordList");
    this.wordList = await res.json();
    console.log(this.wordList);
  };

  getCardContent = async word => {
    const res = await fetch(`/study/cardContent?word=${word}`);
    const result = await res.json();
    console.log(result);
    // Refer to Vue.js
    await app.cards.push(result);
    await initCards();
    let newCard = document.querySelector(
      `.flash-card:nth-child(${this.load + 1})`
    );
    addCardEffect(newCard);
  };

  getNextCard = async () => {
    if (this.load < this.wordList.length) {
      await this.getCardContent(this.wordList[this.load]["id"]);
      this.load++;
    }
  };
}
