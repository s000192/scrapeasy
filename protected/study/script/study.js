const global = {};

window.onload = async () => {
  global.study = new Cards();
  await global.study.initialize();
  setTimeout(() => {
    $("#loading").addClass("d-none");
    $(".flash-cards").toggleClass("d-none d-flex");
  }, 3000);
};
