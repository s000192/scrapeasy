
getUsername = async () => {
    const res = await fetch ('/summary/Username')
    const result = await res.json();

    document.querySelector('#id-box').innerHTML = result;
    document.querySelector('#userName').innerHTML = result;

}

wordsLearned = async () => {
    const res = await fetch ('/summary/wordsLearned')
    const result = await res.json();

    document.querySelector('#words-learned').innerHTML += result;

}

mostVisitedWords = async () => {
    const res = await fetch ('/summary/mostVisitedWords')
    const result = await res.json();

    document.querySelector('#visited-words').innerHTML += result;

}

correctPercentage = async () => {
    const res = await fetch ('/summary/correctPercentage')
    const result = await res.json();

    document.querySelector('#correct-percent').innerHTML += result;

}

top5MostQuizAttempts = async () => {
    const res = await fetch ('/summary/top5MostQuizAttempts')
    const result = await res.json();

    document.querySelector('#mostQuiz-attempt').innerHTML += result;

}

top5MostFailedAttempts = async () => {
    const res = await fetch ('/summary/top5MostFailedAttempts')
    const result = await res.json();

    document.querySelector('#mostFailed-attempt').innerHTML += result;

}

getUsername();
wordsLearned();
mostVisitedWords();
correctPercentage();
top5MostQuizAttempts();
top5MostFailedAttempts();