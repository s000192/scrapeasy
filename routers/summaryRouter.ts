import * as express from "express";
import { SummaryService } from "../services/summaryService";

export class SummaryRouter {
  constructor(private summaryService: SummaryService) {}

  public router = () => {
    const router = express.Router();

    router.get("/", this.summaryPage);
    router.get("/Username", this.getUsername);
    router.get("/wordsLearned", this.wordsLearned);
    router.get("/mostVisitedWords", this.mostVisitedWords);
    router.get("/correctPercentage", this.correctPercentage);
    router.get("/top5MostQuizAttempts", this.top5MostQuizAttempts);
    router.get("/top5MostFailedAttempts", this.top5MostFailedAttempts);
    // router.post("/", this.studyCard);
    return router;
  };

  private summaryPage = async (req: express.Request, res: express.Response) => {
    res.redirect("/summary/summary.html");
  };
  private getUsername = async (req: express.Request, res: express.Response) => {
    if (req.session) {
      res.json(await this.summaryService.showUsername(req.session.currentUser));
    }
  };

  private wordsLearned = async (
    req: express.Request,
    res: express.Response
  ) => {
    if (req.session) {
      res.json(await this.summaryService.wordsLearned(req.session.currentUser));
    }
    // [CODE REVIEW] Prepare a status 401 code and res.json() for those without req.session
  };
  private mostVisitedWords = async (
    req: express.Request,
    res: express.Response
  ) => {
    if (req.session) {
      res.json(
        await this.summaryService.mostVisitedWords(req.session.currentUser)
      );
    }
  };
  private correctPercentage = async (
    req: express.Request,
    res: express.Response
  ) => {
    if (req.session) {
      res.json(
        await this.summaryService.correctPercentage(req.session.currentUser)
      );
    }
  };

  private top5MostQuizAttempts = async (
    req: express.Request,
    res: express.Response
  ) => {
    if (req.session) {
      res.json(
        await this.summaryService.MostQuizAttempts(req.session.currentUser)
      );
    }
  };
  private top5MostFailedAttempts = async (
    req: express.Request,
    res: express.Response
  ) => {
    if (req.session) {
      res.json(
        await this.summaryService.MostFailedAttempts(req.session.currentUser)
      );
    }
  };
}
