import * as express from "express";
// import * as path from "path";
import { Request, Response } from "express";
import { UserService } from "../services/UserService";

export class UserRouter {
  constructor(private userService: UserService) {}

  public router = () => {
    const router = express.Router();
    router.post("/", this.createUser);
    return router;
  };

  private createUser = async (req: Request, res: Response) => {
    try {
      await this.userService.createUser(req.body);
      res.json({ isSuccess: true});
    } catch(e){
      res.json({ isSuccess: false, msg:e });
    }
  };
}
