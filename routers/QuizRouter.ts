import * as express from "express";
import { QuizGenService } from "../services/QuizGenService";
//import * as path from 'path'

export class QuizRouter {
  constructor(private quizGenService: QuizGenService) {}

  public router = () => {
    const router = express.Router();

    router.get("/", this.quizPage);
    router.get("/imgGen", this.quizImageGen);
    router.get("/wrongImg", this.wrongQuizImage);
    router.get("/quizWordList", this.quizWordGenerator);
    router.get("/getImgObj", this.ImgObj);
    router.get("/rightAnswer", this.rightAnswerAdd);
    router.get("/wrongAnswer", this.wrongAnswerAdd);
    // router.post("/", this.studyCard);

    return router;
  };

  private quizPage = async (req: express.Request, res: express.Response) => {
    res.redirect("/quiz/quiz.html");
  };

  private quizImageGen = async (
    req: express.Request,
    res: express.Response
  ) => {
    res.json(await this.quizGenService.quizImageGen("apple"));
  };

  private wrongQuizImage = async (
    req: express.Request,
    res: express.Response
  ) => {
    res.json(await this.quizGenService.wrongQuizImage("apple"));
  };
  
  private quizWordGenerator = async (
    req: express.Request,
    res: express.Response
  ) => {
    if (req.session) {
      res.json(
        await this.quizGenService.quizWordGenerator(req.session.currentUser)
      );
    }
  };

  private ImgObj = async (req: express.Request, res: express.Response) => {
    res.json(await this.quizGenService.quizImgObj(req.query.word));
  };

  private rightAnswerAdd = async (
    req: express.Request,
    res: express.Response
  ) => {
    if (req.session){
    res.json(await this.quizGenService.rightAnswerAdd(req.query.word, req.session.currentUser));
    }
  };

  private wrongAnswerAdd = async (
    req: express.Request,
    res: express.Response
  ) => {
    if(req.session){
    res.json(await this.quizGenService.wrongAnswerAdd(req.query.word, req.session.currentUser));
    }
  };
}
