import * as express from "express";
// import * as path from 'path';
import { ScrapService } from "../services/ScrapService";

export class ScrapRouter {
  constructor(private scrapService: ScrapService) { }

  public router = () => {
    const router = express.Router();

    router.get("/", this.scrapPage);
    router.post("/words", this.logScrapResults);
    router.put("/words", this.saveWords);
    router.delete("/words", this.deleteWords);

    return router;
  };

  private scrapPage = async (
    req: express.Request,
    res: express.Response
  ) => {
    res.redirect('/scrap/scrap.html');
  };

  private logScrapResults = async (
    req: express.Request,
    res: express.Response
  ) => {
    try {
      if (req.session) {
        console.log(req.body.URL);
        console.log(req.session.currentUser);
        res.status(201).json(await this.scrapService.logScrapResults(req.body.URL, req.session.currentUser));
      }
    }
    catch (e) {
      res.json(e);
    }
  };

  private saveWords = async (
    req: express.Request,
    res: express.Response
  ) => {
    try {
      if (req.session) {
        console.log(req.body)
        console.log(req.session.currentUser);
        await this.scrapService.saveWords(req.body, req.session.currentUser);
        res.status(201).send();
      }
    }
    catch (e) {
      res.json(e);
    }
  };

  private deleteWords = async (
    req: express.Request,
    res: express.Response
  ) => {
    try {
      if (req.session) {
        console.log(req.body)
        console.log(req.session.currentUser);
        await this.scrapService.deleteWords(req.body, req.session.currentUser);
        res.status(201).send();
      }
    }
    catch (e) {
      // [CODE REVIEW] Better to provide a status code to distinguish normal and abnormal responses
      res.json(e);
    }
  };
}