import * as express from "express";
import * as path from "path";
import { StudyService } from "../services/StudyService";

export class StudyRouter {
  constructor(private studyService: StudyService) {}

  public router = () => {
    const router = express.Router();

    router.get("/", this.studyPage);
    router.get("/wordList", this.prepareWordList);
    router.get("/cardContent", this.prepareCardContent);
    return router;
  };

  private studyPage = async (
    req: express.Request,
    res: express.Response
  ) => {
    res.sendFile(path.join(__dirname + "/../protected/study/study.html"));
  };

  private prepareWordList = async (
    req: express.Request,
    res: express.Response
  ) => {
    if (req.session){
      res.json(await this.studyService.sortWordsList(req.session.currentUser));
    } else {
      res.status(404).send();
    }
  };

  private prepareCardContent = async (
    req: express.Request,
    res: express.Response
  ) => {
    if (req.session){
      res.json(await this.studyService.accessCardContent(req.query.word, req.session.currentUser));
    } else {
      res.status(404).send();
    }
  };
}