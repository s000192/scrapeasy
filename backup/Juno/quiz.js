

class Quiz {
    constructor() {
        //this.count = -1;
        //this.wordList = [];
        this.imgObj = {};
    }

    randomQuizWord = async () => {
        let num = Math.floor((Math.random() * quizGame.wordList.length));
        document.querySelector('#quiz-word h3').innerHTML = quizGame.wordList[num];
        console.log(quizGame.wordList[num]);
        return quizGame.wordList[num];
    }

    getQuizImgObj = async (word) => {
        const res = await fetch(`/quiz/getImgObj?word=${word}`)
        const result = await res.clone().json();
        this.imgObj = result;

        document.querySelector('#picture-1 img').removeAttribute('correct');
        document.querySelector('#picture-1 img').removeAttribute('wrong');
        document.querySelector('#picture-2 img').removeAttribute('correct');
        document.querySelector('#picture-2 img').removeAttribute('wrong');

        let num = Math.random();
        if (num < 0.5) {
            document.querySelector('#picture-1 img').src = result.correct;
            document.querySelector('#picture-1 img').setAttribute('correct', word);
            document.querySelector('#picture-2 img').src = result.wrong;
            document.querySelector('#picture-2 img').setAttribute('wrong', word);
        } else {
            document.querySelector('#picture-1 img').src = result.wrong;
            document.querySelector('#picture-1 img').setAttribute('wrong', word);
            document.querySelector('#picture-2 img').src = result.correct;
            document.querySelector('#picture-2 img').setAttribute('correct', word);
        }
        return console.log(result);
    }

}

class QuizGame {
    constructor() {
        this.count = 0;
        this.wordList = null;
    }
    getWordList = async () => {
        const res = await fetch('/quiz/quizWordList');
        const result = await res.clone().json();

        if (this.wordList == null) {
            return this.wordList = result;
        } else {
            return;
        }
    };

    nextQuestion = async () => {
        await this.getWordList();
        if (this.count < 5 ) {
            await quiz.randomQuizWord();
            const word = await quiz.randomQuizWord();
            await quiz.getQuizImgObj(word);
            wordArr.push(word);
            const index = this.wordList.indexOf(word)
            this.wordList.splice(index, 1);
            this.count++;
            document.querySelector('#quiz-word span').innerHTML = '';
            console.log(this.wordList);
            aCount = 0;
        } else if (this.count = 5 ) {
            document.querySelector('#nextButton').innerHTML = 'QUIZ COMPLETED';
            for (let word of wordArr) {
                document.querySelector('#qRow').innerHTML += `<th colspan="1" >${word}</th>`
                
            }
            for (let CR of CRArr) {
                document.querySelector('#aRow').innerHTML += `<th colspan="1" >${CR}</th>`
            }
            console.log(wordArr);
            console.log(CRArr);
            return;
        }
        

    }

    checkAnswerPic1 = async () => {
        if (document.querySelector('#picture-1 img').getAttribute('correct') !== null) {

            document.querySelector('#quiz-word span').innerHTML = 'CORRECT!'
            const word = document.querySelector('#picture-1 img').getAttribute('correct')
            const res = await fetch(`/quiz/rightAnswer?word=${word}`)
            const result = await res.clone().json();
            CRArr.push('CORRECT!');

        }
        if (document.querySelector('#picture-1 img').getAttribute('wrong') !== null) {

            document.querySelector('#quiz-word span').innerHTML = 'WRONG!'
            const word = document.querySelector('#picture-1 img').getAttribute('wrong')
            const res = await fetch(`/quiz/wrongAnswer?word=${word}`)
            const result = await res.clone().json();
            CRArr.push('WRONG!');

        }

    }
    checkAnswerPic2 = async () => {
        if (document.querySelector('#picture-2 img').getAttribute('correct') !== null) {

            document.querySelector('#quiz-word span').innerHTML = 'CORRECT!';
            const word = document.querySelector('#picture-2 img').getAttribute('correct')
            const res = await fetch(`/quiz/rightAnswer?word=${word}`)
            const result = await res.clone().json();
            CRArr.push('CORRECT!');

        }
        if (document.querySelector('#picture-2 img').getAttribute('wrong') !== null) {

            document.querySelector('#quiz-word span').innerHTML = 'WRONG!';
            const word = document.querySelector('#picture-2 img').getAttribute('wrong')
            const res = await fetch(`/quiz/wrongAnswer?word=${word}`)
            const result = await res.clone().json();
            CRArr.push('WRONG!');
        }

    }




}

getUsername = async () => {
    const res = await fetch ('/summary/Username')
    const result = await res.json();

    document.querySelector('#userName').innerHTML = result;

}

getUsername();

const quiz = new Quiz();
const quizGame = new QuizGame();
let aCount = 0;
let wordArr = [];
let CRArr = [];
let CRPercent = 



//quiz.getWordList();
//async function newGame (){
//    await getWordList();
//    
//}

//newGame ();
//quizGame.getWordList();
//quizGame.nextQuestion();
//console.log(quizGame.wordList);
//console.log(count);

//next button
document.querySelector('#nextButton').addEventListener("click", quizGame.nextQuestion);
//document.querySelector('#nextButton').addEventListener("click", quizGame.nextQuestion);

//choose answer
document.querySelector('#picture-1 img').addEventListener("click", () => {
    if (aCount < 1) { quizGame.checkAnswerPic1(); };
    aCount++;
});
document.querySelector('#picture-2 img').addEventListener("click", () => {
    if (aCount < 1) { quizGame.checkAnswerPic2(); };
    aCount++;
});

window.onload = async () => {
    await quizGame.nextQuestion();
    if (quizGame.wordList.length < 4) {
        document.querySelector('#quiz-word').innerHTML = 
        'Please save more words into your word library in order to start challanging yourself! Redirecting to Scrap page in 5 seconds';
        setTimeout(() => {window.location = '/scrap/'},5000);
    } else {
    
    wordArr = [];
    CRArr = [];
    }
    console.log(quizGame.wordList);
}
