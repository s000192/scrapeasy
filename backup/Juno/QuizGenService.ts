import * as jsonfile from "jsonfile";
import * as path from "path";
import { imageSearch } from "./imageSearchAPI";

const JSON_FILE = path.join(__dirname, "wordBank.json");
const USER_FILE = path.join(__dirname, "users.json");

//async function checkWords(word: string) {
//    const
//}

export class QuizGenService {
    constructor() { }

    async quizImageGen(word: string) {
        let url = [];
        const wordBank = await jsonfile.readFile(JSON_FILE);
        const checkWords = "a";
        if (checkWords) {
            //there is existing link in json file
            for (let wordBankWord of wordBank) {
                if (wordBankWord.id == word) {
                    return wordBankWord.url;
                }
            }
        } else {
            //no image link in json file
            const result = imageSearch(word);
            return result;
        }
    }

    async wrongQuizImage(word: string) {
        const wordBank = await jsonfile.readFile(JSON_FILE);
        //every word in wordBank
        for (let wordBankWord of wordBank) {
            //skip if input == id
            if (wordBankWord.id == word) {
                continue;
            } else {
                for (let i = 0; i < wordBank.length; i++) {
                    let num = Math.floor(Math.random() * wordBank.length);
                    if (wordBank[num].url) {
                        return wordBank[num].url;
                    } else {
                        continue;
                    }
                }
            }
        }
    }

    async quizWordGenerator(currentUser: string) {
        let acqWordsArr: string[] = [];
        let quizArr: string[] = [];
        const UserWordBank: object[] = await jsonfile.readFile(USER_FILE);
        const userData = UserWordBank.find(
            (user) => user["username"] === currentUser
        );

        if (userData) {
            const acquiredWords =
                UserWordBank[UserWordBank.indexOf(userData)]["acquiredWords"];
            for (let item in acquiredWords) {
                acqWordsArr.push(item);
            }
            console.log(acqWordsArr);
            if (acqWordsArr.length >= 5) {
                while (quizArr.length < 5) {
                    let num = Math.floor(Math.random() * Object.keys(acquiredWords).length);
                    if (quizArr.length < 1) {
                        quizArr.push(acqWordsArr[num]);
                    } else {
                        if (quizArr.indexOf(acqWordsArr[num]) == -1) {
                            quizArr.push(acqWordsArr[num]);
                        } else {
                            continue;
                        }
                    }
                }
            } else {
                while (quizArr.length < acqWordsArr.length) {
                    let num = Math.floor(Math.random() * Object.keys(acquiredWords).length);
                    if (quizArr.length < 1) {
                        quizArr.push(acqWordsArr[num]);
                    } else {
                        if (quizArr.indexOf(acqWordsArr[num]) == -1) {
                            quizArr.push(acqWordsArr[num]);
                        } else {
                            continue;
                        }
                    }
                }
            }

        }
        console.log(quizArr);
        return quizArr;
    }

    async quizImgObj(word: string): Promise<any> {
        const correctLink = await this.quizImageGen(word);
        const wrongLink = await this.wrongQuizImage(word);

        const imgPair = { correct: correctLink, wrong: wrongLink };

        if (correctLink == wrongLink) {
            return this.quizImgObj(word);
        } else {
            console.log(imgPair);
            return imgPair;
        }
    }

    async rightAnswerAdd(word: string) {
        const wordBank = await jsonfile.readFile(USER_FILE);
        const acquiredWords = wordBank[0]["acquiredWords"];
        for (let bankWord in acquiredWords) {
            if (bankWord == word) {
                acquiredWords[bankWord]["attempted"] += 1;
                acquiredWords[bankWord]["correct"] += 1;
                await jsonfile.writeFile(USER_FILE, wordBank);
                console.log(acquiredWords[bankWord]);
                return acquiredWords[bankWord];
            }
        }
    }

    async wrongAnswerAdd(word: string) {
        const wordBank = await jsonfile.readFile(USER_FILE);
        const acquiredWords = wordBank[0]["acquiredWords"];
        for (let bankWord in acquiredWords) {
            if (bankWord == word) {
                acquiredWords[bankWord]["attempted"] += 1;
                await jsonfile.writeFile(USER_FILE, wordBank);
                console.log(acquiredWords[bankWord]);
                return acquiredWords[bankWord];
            }
        }
    }
}

//const quizGenService = new QuizGenService();

//quizImageGen('banana');
//wrongQuizImage('banana');
//quizGenService.quizWordGenerator('jackson');
//quizGenService.quizImgObj('apple');
//quizGenService.rightAnswerAdd('apple');
//quizGenService.wrongAnswerAdd('apple');
